# NODEJS_HW3

## Getting started

This is a small UBER-like service for trucks. This project includes API and simple UI.

## Project start

To start a project in a prod mode type this command:

```
npm start
```

To start a project in a dev mode type this command:

```
npm run build
```

Server starts on a 8080 port by default. You can change it in .env file if you want.